<?php
// voor een goed geordende url
Class Http
{
    public static $webroot = '';

    public static function boot()
    {
        // zoeken naar localhost
        // zoeken naar /public/
        // webroot van maken

        if($_SERVER['HTTP_HOST'] == 'localhost' && strpos($_SERVER['REQUEST_URI'], '/public/')) {

            $urlParts = explode('/public/', $_SERVER['REQUEST_URI']);

            self::$webroot = self::httpOrHttps().$_SERVER['HTTP_HOST'].$urlParts[0].'/public/';
        }
        else {
            self::$webroot = $_SERVER['HTTP_HOST'];
        }

    }

    public static function webroot()
    {
        return self::$webroot;
    }
    // checken of het domein op een http of https server is
    private static function httpOrHttps()
    {
        if(isset($_SERVER['HTTPS'])) {
            return 'https://';
        }
        return 'http://';
    }

}
