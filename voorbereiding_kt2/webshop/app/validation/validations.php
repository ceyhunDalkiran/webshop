<?php
// In deze file wordt de error-handling aangemaakt. Ook wordt er gecheckt of alle input juist is etc met functies
$errors = [];
// validatie functienamen
$validations = [
    'required',
    'number',
    'email',
    'name',
    'min',
    'max',
    'confirmed',
    'postcode',
    'uniqueEmailUserRegister',
];

// error handling
foreach($variables as $key => $checks) {

    // dd($checks);

    foreach($checks as $check) {

        // dd($check);

        $checkExploded = explode(':', $check);

        if(count($checkExploded) > 1) {


            // wel een :
            $checkFunction = 'is'.ucfirst($checkExploded[0]);
            if($error = $checkFunction($_POST[$key], $checkExploded[1], $key, $checks)) {

                if(array_key_exists($key, $errors)) {
                    array_push($errors[$key], $error);
                }
                else {
                    $errors[$key] = [$error];
                }
            }

        }
        else {
            // geen :
            $checkFunction = 'is'.ucfirst($check);

            if($error = $checkFunction($_POST[$key], $key, $checks)) {


                if(array_key_exists($key, $errors)) {
                    array_push($errors[$key], $error);
                }
                else {
                    $errors[$key] = [$error];
                }
            }
        }
    }
}

// verplichte veld valideren
function isRequired($value, $key, $checks)
{
    if(! $value) {
        return 'U heeft geen waarde ingevuld';
    }
}

// verplichte getal validatie
function isNumber($value, $key, $checks)
{
    if($value && ! is_numeric($value)) {
        return 'U heeft geen numerieke waarde ingevuld';
    }
}

// postcode validatie met regexr
function isPostcode($value, $key, $checks)
{
    if($value && ! preg_match('/^[1-9][0-9]{3}[\s]?[A-Za-z]{2}$/i', $value)) {
        return 'U heeft geen correcte postcode ingevuld';
    }
}

// email validatie met regexr
function isEmail($value, $key, $checks)
{
    if($value && ! preg_match('/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i', $value)) {
        return 'U heeft geen geldig e-mail adres ingevuld';
    }
}

// naam validatie
function isName($value, $key, $checks)
{
    if($value && ! preg_match('/^[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.\'-]+$/u', $value)) {
        return 'U heeft geen geldige waarde ingevuld';
    }
}

// bevestigings check
function isConfirmed($value, $key, $checks)
{
    if($value && $_POST[$key] != $_POST[$key.'_confirmed']) {
        return 'Het confirmatie veld heeft niet dezelfde waarde';
    }
}

// minimum aantal karakters check
function isMin($value, $amount, $key, $checks)
{
    if($value && strlen($value) < $amount) {
        return 'U heeft niet voldoende tekens ingevoerd. Minimaal '.$amount;
    }
}

// maximum aantal karakters check
function isMax($value, $amount, $key, $checks)
{
    if($value && strlen($value) > $amount) {
        return 'U heeft teveel tekens ingevoerd. Maximaal '.$amount;
    }
}

// validatie om te checken of er geregistreerd wordt met een bestaande email
function isUniqueEmailUserRegister($value, $key, $checks)
{
    $connection = db();
    $query = "SELECT email FROM users WHERE email = :email";
    $user = $connection->prepare($query);
    $user->execute([
        'email' => $value
    ]);
    $resultsUserCheck = $user->fetch(PDO::FETCH_ASSOC);

    if ($resultsUserCheck) {
       return "Gebruiker met de ingevoerde email bestaat al";
   }
}
