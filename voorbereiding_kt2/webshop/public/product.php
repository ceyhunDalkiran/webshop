<?php
//verbinding maken met database
require '../boot.php';

    $connection = db();
    $product = $connection->prepare('SELECT * FROM products WHERE slug = :slug'); //query voorbereiden waarbij alles geselecteerd wordt waar de slug met de gebinde slug overeenkomt

try{
    $product->execute([
        'slug' => $_GET['slug'] // slug wordt gebind
    ]);
    $product->setFetchMode(PDO::FETCH_ASSOC);
    $product = $product->fetch();
}
catch(PDOException $e){
    dd($e->getMessage());
}

//404 pagina instellen, als slug er niet is dan wordt er verwezen naar 404.php
if(!$product['slug'])
{
    header("Location: ../404.php");
}

 ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>GamingMuizen.nl</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <!-- Winkelmand inladen zonder de pagina te refreshen(automatisch inladen) met jquery AJAX calls -->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo asset('css/style.css'); ?>">
    <script src="<?php echo asset('js/webshop.js'); ?>"></script>
    <script src="<?php echo asset('js/shoppingcart.js'); ?>"></script>
<style>
@media (max-width: 600px) {
.container{
    padding: 30px 50px;
  }
}
.text-center{
    bottom:0;
    position: fixed;
    left: 0;
    width: 100%;
}
#bucket{
    float:right;
    padding-top: 60px;
    padding-right: 15px;
}
#add{
    margin-left: 250px;
    padding: 20px 25px;
}
</style>
</head>

<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="50">
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">Home</a>
            </div>
            <div class="collapse navbar-collapse" id="myNavbar">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href=""><?php if(@$_SESSION['user']) { ?>
                        <li><?php echo strtoupper($_SESSION['user']['first_name']); ?></li>
                    <?php } ?></a></li>
                    <!-- Als er een user is dan laat hij de voornaam naast de account icoon zien en kan de user naar zijn account pagina gaan, als er user niet ingelogd is dan ziet de user zijn naam niet en gaat de user naar inlog pagina. -->
                    <li><a href="<?php if(@$_SESSION['user']) { echo asset('account.php');} else{ echo asset('login.php');}?>" class="fa fa-user" style="font-size:24px"></a></li>
                        <li><a href="<?php echo asset('producten.php'); ?>">PRODUCTEN</a></li>
                </ul>
            </div>
        </div>
    </nav>

    <!-- Winkelmand (is bucket.php) -->
    <div id="bucket">
        <?php include 'partials/bucket.php'; ?>
    </div>

<div class="container">
    <div class="row">
        <!-- Hier worden de gegevens van het geklikte product weergegeven, dankzij templating -->
        <h1><?php echo $product['title']?></h1>
        <img alt="product_image" class="img-thumbnail"  width="400px" height="400px" src="../<?php echo $product['image']?>">
        <button type="button" id="add" class="w3-button w3-black w3-padding-large w3-large btn btn-warning add-to-cart" data-url="<?php echo asset("cart/add.php?id="); echo $product['id'];?>">Voeg in Winkelmand<br/></button><br>
        <p><h1>€<?php echo $product['price']?></h1></p><br>
        <p><?php echo $product['description']?></p><br>
        <a class="btn btn-outline-dark" href="../producten.php">Terug naar shop</a>
    </div>
</div>

<!-- Footer -->
<footer class="text-center">
    <p>GamingMuizen© 2018</p>
</footer>

</body>
</html>
