<?php
    // connectie maken
    require '../boot.php';
?>
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo asset('css/style.css'); ?>">
    <script src="<?php echo asset('js/webshop.js'); ?>"></script>
<style>
@media (max-width: 600px) {
.container{
    padding: 100px 60px;
  }
}
.text-center{
    bottom:0;
    position: fixed;
    left: 0;
    width: 100%;
}
#logout{
    float:right;
}
</style>
</head>

<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="50">
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            <a class="navbar-brand" href="<?php echo asset('index.php'); ?>">Home</a>
            </div>
            <div class="collapse navbar-collapse" id="myNavbar">
                <ul class="nav navbar-nav navbar-right">
                    <!-- Als er een user is dan laat de voornaam zien-->
                    <li><a href=""><?php if(@$_SESSION['user']) { ?>
                        <li><?php echo strtoupper($_SESSION['user']['first_name']); ?></li>
                    <?php } ?></a></li>
                    <!-- als user ingelogd is ga naar account.php NIET INGELOGD-> ga naar login pagina-->
                    <li><a href="<?php if(@$_SESSION['user']) { echo asset('account.php');} else{ echo asset('login.php');}?>" class="fa fa-user" style="font-size:24px"></a></li>
                    <li><a href="<?php echo asset('producten.php'); ?>">PRODUCTEN</a></li>
                </ul>
            </div>
        </div>
    </nav>

<div class="container">
    <h1>Account gegevens</h1>
    <a href="logout.php" id="logout" class="btn btn-info btn-sm">
        <span class="glyphicon glyphicon-log-out"></span><br>Uitloggen
    </a>
    <a href="wijzigAccount.php">Gegevens wijzigen?</a><br><br>
    <!--Hier worden alle gegevens van de user gepreviewd vanuit de session-->
    <?php echo 'Voornaam: ' . $_SESSION['user']['first_name'] .' <br> Achternaam: '. $_SESSION['user']['last_name'] . '<br>Email: ' . $_SESSION['user']['email'] . '<br>Land: ' . $_SESSION['user']['country'] . '<br>Stad: ' .
    $_SESSION['user']['city'] . '<br>Straat: ' . $_SESSION['user']['street'] .'<br>Straatnummer: ' . $_SESSION['user']['street_number'] . '<br>Postcode: ' . $_SESSION['user']['zipcode'] . '<br>Datum account aangemaakt: ' . $_SESSION['user']['created_at']?>
</div>

<!-- Footer -->
<footer class="text-center">
    <p>GamingMuizen© 2018</p>
</footer>

</body>
