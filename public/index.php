<?php
    // connectie maken
    require '../boot.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>GamingMuizen.nl</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo asset('css/style.css'); ?>">
    <script src="<?php echo asset('js/webshop.js'); ?>"></script>
<style>
@media (max-width: 600px) {
.container{
    padding: 30px 50px;
  }
.backgroundImage{
    padding:50px 0px;
  }
}
</style>
</head>

<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="50">
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo asset('index.php'); ?>">Home</a>
            </div>
            <div class="collapse navbar-collapse" id="myNavbar">
                <ul class="nav navbar-nav navbar-right">
                    <!-- Als er een user is dan laat hij de voornaam naast de account icoon zien en kan de user naar zijn account pagina gaan, als er user niet ingelogd is dan ziet de user zijn naam niet en gaat de user naar inlog pagina. -->
                    <li><a href=""><?php if(@$_SESSION['user']) { ?>
                        <li><?php echo strtoupper($_SESSION['user']['first_name']); ?></li>
                    <?php } ?></a></li>
                    <li><a href="<?php if(@$_SESSION['user']) { echo asset('account.php');} else{ echo asset('login.php');}?>" class="fa fa-user" style="font-size:24px"></a></li>
                    <li><a href="<?php echo asset('producten.php'); ?>">PRODUCTEN</a></li>
                </ul>
            </div>
        </div>
    </nav>

<div class="backgroundImage">
    <img src=<?php echo asset("images/Razer-Background.jpg");?> alt="background_image" width="100%" id="background-image-razer">
</div>


<!-- Container (The Band Section) -->
<div id="band" class="container text-center">
    <h3>OVER ONS</h3>
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
    <br>
    <div class="row">
        <div class="col-sm-4">
            <p class="text-center"><strong>Voor 23.59 besteld?<br>Morgen in huis!</strong></p><br>
            <img src="images/bezorging.png" class="img-circle person" alt="Random Name" width="255" height="255">
        </div>
        <div class="col-sm-4">
            <p class="text-center"><strong>Beste gaming muizen<br>Van 2018</strong></p><br>
            <img src="images/kwaliteit.png" class="img-circle person" alt="Random Name" width="255" height="255">
        </div>
        <div class="col-sm-4">
            <p class="text-center"><strong>Hulp nodig?<br>Neem contact op!</strong></p><br>
            <img src="images/help.png" class="img-circle person" alt="Random Name" width="255" height="255">
        </div>

    </div>
</div>

<!-- Footer -->
<footer class="text-center">
    <p>GamingMuizen© 2018</p>
</footer>

</body>
</html>
