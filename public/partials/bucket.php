<!-- De bucket is de winkelmand, hier in wordt getemplated met php, titel, aantal, prijs wordt weergegeven. Er producten verwijderd en toegevoegd worden. Als er op afrekenen wordt gedrukt, dan wordt er verwezen naar de order pagina-->
<h2>Winkelmand</h2>

<table class="table">
    <thead>
        <tr>
            <th>Titel</th>
            <th>Aantal</th>
            <th>Prijs</th>
        </tr>
    </thead>
    <tbody>
    <!-- Templating en loopen door product-->
    <?php foreach($_SESSION['cart']['products'] as $cartProduct) { ?>
        <tr>
            <td><?php echo $cartProduct['title']; ?></td>
            <td><?php echo $cartProduct['quantity']; ?></td>
            <td><?php echo $cartProduct['price']; ?></td>
            <td><button type="button" class="w3-button w3-black w3-padding-large w3-large btn btn-warning add-to-cart" data-url="<?php echo asset('cart/remove.php?id='.$cartProduct["id"]); ?>">-<br/></button></td>
            <td><button type="button" class="w3-button w3-black w3-padding-large w3-large btn btn-warning add-to-cart" data-url="<?php echo asset('cart/add.php?id='.$cartProduct["id"]); ?>">+<br/></button></td>
        </tr>
    <?php } ?>
    </tbody>
</table>

<h2>Totaal: <?php echo $_SESSION['cart']['total']; ?></h2>
<a href="<?php echo asset('../public/order.php'); ?>"><button type="button" class="w3-button w3-black w3-padding-large w3-large btn">Afrekenen</button></a>
