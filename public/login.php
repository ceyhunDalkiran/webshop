<?php
    // connectie
    require '../boot.php';
        // als de form post method heeft
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            // validatie eigenschappen
            $variables = [
                'email' => ['required', 'email', 'min:9', 'max:150'],
                'password' => ['required', 'min:8', 'max:150'],
            ];
        // valideer (check alles met validations.php)
        require '../app/validation/validations.php';
        // als er geen errors zijn:
        if(count($errors) == 0) {

            // check email
            $connection = db();
            $query = "SELECT * FROM users WHERE email = :email";
            $user = $connection->prepare($query);
            $user->execute([
                // bind parameters
                'email' => $_POST['email'],
            ]);
            $user = $user->fetch(PDO::FETCH_ASSOC);

            if(! $user) {
                // deze user is niet bij ons bekend met dit email adres
            }
            // check wachtwoord
            if (password_verify($_POST['password'], $user['password'])) {
                // stop in sessie, u bent ingelogd
                $_SESSION['user'] = $user;
                header("location:index.php");
               }else {
                $errors['main'] = 'Gegevens zijn incorrect'; // error als gegevens incorrect zijn
           }

        }


    }

    function value($key)
        {
            return @$_POST[$key];
        }

?>
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo asset('css/style.css'); ?>">
    <script src="<?php echo asset('js/webshop.js'); ?>"></script>
<style>
@media (max-width: 600px) {
.container{
    padding: 100px 60px;
  }
}
.text-center{
    bottom:0;
    position: fixed;
    left: 0;
    width: 100%;
}

</style>
</head>

<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="50">
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo asset('index.php');?>">Home</a>
            </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav navbar-right">
                <!-- Als er een user is dan laat de voornaam zien-->
                <?php if(@$_SESSION['user']) { ?>
                    <li><?php echo $_SESSION['user']['first_name']; ?></li>
                <?php } ?>
                <!-- Als er een user is dan laat hij de voornaam naast de account icoon zien en kan de user naar zijn account pagina gaan, als er user niet ingelogd is dan ziet de user zijn naam niet(LOGISCH) en gaat de user naar inlog pagina. -->
                <li><a href="<?php if(@$_SESSION['user']    ) { echo asset('account.php');} else{ echo asset('login.php');}?>" class="fa fa-user" style="font-size:24px"></a></li>
                <li><a href="<?php echo asset('producten.php'); ?>">PRODUCTEN</a></li>
            </ul>
        </div>
        </div>
    </nav>

<div class="container">
    <form action="" method="POST">
        <?php if(@$errors['main']) { echo $errors['main']; } ?> <!-- laat error zien -->
      <div class="form-row">
              <div class="form-group col-md-3">
                <label for="inputEmail4">Email</label>
                <!-- als er gerefresht wordt, laat de value(je ingevoerde input) zien-->
                <input type="email" class="form-control" id="inputEmail4" placeholder="Email" name="email" value="<?php echo value('email'); ?>">
                <!-- errors laten zien(error-handling) -->
                <?php echo (@$errors['email']) ? '<p class="text-danger">'.$errors['email'][0].'</p>' : ''; ?>
              </div>
          <div class="form-group col-md-3">
                <label for="inputPassword4">Wachtwoord</label>
                <input type="password" class="form-control" id="inputPassword4" placeholder="Wachtwoord" name="password">
                <?php echo (@$errors['password']) ? '<p class="text-danger">'.$errors['password'][0].'</p>' : ''; ?>
          </div>
          <div class="form-group col-md-4">
                <button type="submit" class="btn btn-primary">Inloggen</button>
          </div>
      </div>
    </form>
</div>

<!-- Footer -->
<footer class="text-center">
    <p>GamingMuizen© 2018</p>
</footer>

</body>
