<?php
    //connectie maken
    require '../boot.php';
    // Als de request method POST is:
     if ($_SERVER['REQUEST_METHOD'] === 'POST') {
         //validatie eigenschappen
         $variables = [
             'first_name' => ['required', 'name', 'min:2', 'max:25'],
             'suffix_name' => ['name', 'min:2', 'max:15'],
             'last_name' => ['required', 'name', 'min:2', 'max:50'],
             'country' => ['required', 'name', 'min:2', 'max:15'],
             'city' => ['required', 'name', 'min:2', 'max:70'],
             'street' => ['required', 'name', 'min:2', 'max:70'],
             'street_number' => ['required', 'number', 'min:1', 'max:5'],
             'street_suffix' => ['min:1', 'max:50'],
             'zipcode' => ['required', 'min:6', 'max:7', 'postcode'],
             'email' => ['required', 'email', 'min:9', 'max:150'],
             'password' => ['required', 'confirmed', 'min:8', 'max:150'],
         ];
        // valideer (ga naar validations.php en valideer)
        require '../app/validation/validations.php';

        // Als er geen errors zijn:
        if(count($errors) == 0)
        {
            try { // gegevens updaten met update query waarbij de oude gegevens aangepast worden met de nieuwe
                $connection = db();
                $query = "UPDATE users SET first_name='".$_POST['first_name']."', suffix_name='".$_POST['suffix_name']."', last_name='".$_POST['last_name']."', country='".$_POST['country']."', city='".$_POST['city']."', street='".$_POST['street']."', street_number='".$_POST['street_number']."'
                , street_suffix='".$_POST['street_suffix']."', zipcode='".$_POST['zipcode']."', email='".$_POST['email']."', password='".$_POST['password']."', WHERE id ='". $_SESSION['user']['id']."' ";
            }
            catch(Exception $e){
                echo "Error: " . $e->getMessage();
            }
        }
    }

    function value($key)
    {
        return @$_POST[$key];
    }
 ?>
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo asset('css/style.css'); ?>">
    <script src="<?php echo asset('js/webshop.js'); ?>"></script>
<style>
@media (max-width: 600px) {
.container{
    padding: 100px 60px;
  }
}
.text-center{
    bottom:0;
    position: fixed;
    left: 0;
    width: 100%;
}

</style>
</head>

<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="50">
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo asset('index.php'); ?>">Home</a>
            </div>
            <div class="collapse navbar-collapse" id="myNavbar">
                <ul class="nav navbar-nav navbar-right">
                    <!-- Als er een user is(ingelogd) laat voornaam zien -->
                    <li><a href=""><?php if(@$_SESSION['user']) { ?>
                        <li><?php echo strtoupper($_SESSION['user']['first_name']); ?></li>
                    <?php } ?></a></li>
                    <!-- Als user ingelogd is laat linken naar account.php zo niet(niet ingelogd) link naar login pagina -->
                    <li><a href="<?php if(@$_SESSION['user']) { echo asset('account.php');} else{ echo asset('login.php');}?>" class="fa fa-user" style="font-size:24px"></a></li>
                    <li><a href="<?php echo asset('producten.php'); ?>">PRODUCTEN</a></li>
                </ul>
            </div>
        </div>
    </nav>

<div class="container">
    <form action="" method="POST">
        <div class="form-row">
            <div class="form-group col-md-3">
                <label for="first_name">Voornaam</label>
                <!-- Als er gerefresht wordt, retourneer de input -->
                <input type="text" class="form-control" id="first_name" placeholder="Voornaam" name="first_name" value="<?php echo value('first_name'); ?>">
                <!-- Error laten zien (error handling)-->
                <?php echo (@$errors['first_name']) ? '<p class="text-danger">'.$errors['first_name'][0].'</p>' : ''; ?>
            </div>
            <div class="form-group col-md-2">
                <label for="suffixname">Tussenvoegsel</label>
                <input type="text" class="form-control" id="inputSuffixname" placeholder="Tussenvoegsel" name="suffix_name" value="<?php echo value('suffix_name'); ?>">
                <?php echo (@$errors['suffix_name']) ? '<p class="text-danger">'.$errors['suffix_name'][0].'</p>' : ''; ?>
            </div>
            <div class="form-group col-md-3">
                <label for="lastname">Achternaam</label>
                <input type="text" class="form-control" id="inputLastname" placeholder="Achternaam" name="last_name" value="<?php echo value('last_name'); ?>">
                <?php echo (@$errors['last_name']) ? '<p class="text-danger">'.$errors['last_name'][0].'</p>' : ''; ?>
            </div>
            <div class="form-group col-md-3">
                <label for="inputCountry">Land</label>
                <input type="text" class="form-control" id="inputCountry" placeholder="Land" name="country" value="<?php echo value('country'); ?>">
                <?php echo (@$errors['country']) ? '<p class="text-danger">'.$errors['country'][0].'</p>' : ''; ?>
            </div>
            <div class="form-group col-md-4">
                <label for="inputCity">Stad</label>
                <input type="text" class="form-control" id="inputCity" placeholder="Stadsnaam" name="city" value="<?php echo value('city'); ?>">
                <?php echo (@$errors['city']) ? '<p class="text-danger">'.$errors['city'][0].'</p>' : ''; ?>
            </div>
            <div class="form-group col-md-4">
                <label for="inputStreetname">Straat</label>
                <input type="text" class="form-control" id="inputStreetname" placeholder="Straatnaam" name="street" value="<?php echo value('street'); ?>">
                <?php echo (@$errors['street']) ? '<p class="text-danger">'.$errors['street'][0].'</p>' : ''; ?>
            </div>
            <div class="form-group col-md-3">
                <label for="inputStreetnumber">Straatnummer</label>
                <input type="text" class="form-control" id="inputStreetnameSuffix" name="street_number" value="<?php echo value('street_number'); ?>">
                <?php echo (@$errors['street_number']) ? '<p class="text-danger">'.$errors['street_number'][0].'</p>' : ''; ?>
            </div>
            <div class="form-group col-md-3">
                <label for="inputStreetnameSuffix">Straat tussenvoegsel</label>
                <input type="text" class="form-control" id="inputStreetnameSuffix" name="street_suffix" value="<?php echo value('street_suffix'); ?>">
                <?php echo (@$errors['street_suffix']) ? '<p class="text-danger">'.$errors['street_suffix'][0].'</p>' : ''; ?>
            </div>
        </div>

        <div class="form-row">
            <div class="form-group col-md-2">
                <label for="inputZip">Postcode</label>
                <input type="text" class="form-control" id="inputZip" name="zipcode" value="<?php echo value('zipcode'); ?>">
                <?php echo (@$errors['zipcode']) ? '<p class="text-danger">'.$errors['zipcode'][0].'</p>' : ''; ?>
            </div>
            <div class="form-group col-md-3">
                <label for="inputEmail4">Email</label>
                <input type="email" class="form-control" id="inputEmail4" placeholder="Email" name="email" value="<?php echo value('email'); ?>">
                <?php echo (@$errors['email']) ? '<p class="text-danger">'.$errors['email'][0].'</p>' : ''; ?>
            </div>
            <div class="form-group col-md-3">
                <label for="inputPassword4">Huidige Wachtwoord</label>
                <input type="password" class="form-control" id="inputPassword4" placeholder="Huidige Wachtwoord" name="password">
                <?php echo (@$errors['password']) ? '<p class="text-danger">'.$errors['password'][0].'</p>' : ''; ?>
            </div>
            <div class="form-group col-md-3">
                <label for="inputPassword4">Nieuwe Wachtwoord</label>
                <input type="password" class="form-control" id="inputPassword4" placeholder="Nieuwe Wachtwoord" name="password">
                <?php echo (@$errors['password_confirmed']) ? '<p class="text-danger">'.$errors['password'][0].'</p>' : ''; ?>
            </div>
            <div class="form-group col-md-3">
                <label for="inputPassword4">Herhaal Nieuwe Wachtwoord</label>
                <input type="password" class="form-control" id="inputPassword4" placeholder="Herhaal nieuwe Wachtwoord" name="password_confirmed">
                <?php echo (@$errors['password_confirmed']) ? '<p class="text-danger">'.$errors['password_confirmed'][0].'</p>' : ''; ?>
            </div>
            <div class="form-group col-md-4">
                <button type="submit" class="btn btn-primary">Wijzig</button>
            </div>
        </div>

    </form>
</div>

<!-- Footer -->
<footer class="text-center">
    <p>GamingMuizen© 2018</p>
</footer>

</body>
